'use strict';

/**
 * @ngdoc service
 * @name nanopoolFrontendApp.Dummy
 * @description
 * # Dummy
 * Service in the nanopoolFrontendApp.
 */
angular.module('nanopoolFrontendApp')
  .service('Dummy', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
