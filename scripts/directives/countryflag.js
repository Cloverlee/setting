'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:countryFlag
 * @description
 * # countryFlag
 */
angular.module('nanopoolFrontendApp')
  .directive('countryFlag', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/directiveTemplates/countryFlag.html',
      scope: {
        countryName: "=",
        countryCode: "=",
        s3Url: "="
      }
    };
  });
