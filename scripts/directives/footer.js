'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:footer
 * @description
 * # footer
 */
angular.module('nanopoolFrontendApp')
  .directive('mainFooter', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/common/footer.html',
      scope: true,
      // controller: 'FooterCtrl'
    };
  });
