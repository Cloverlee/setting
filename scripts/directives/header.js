'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:header
 * @description
 * # header
 */
angular.module('nanopoolFrontendApp')
  .directive('mainHeader', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/common/header.html',
      scope: true,
      controller: 'HeaderCtrl'
    };
  });
