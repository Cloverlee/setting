'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:SuccessCtrl
 * @description
 * # SuccessCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('SuccessCtrl', function ($scope, config) {

    $scope.s3Url = config.s3BucketUrl;

  });
