'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:SupportCtrl
 * @description
 * # SupportCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('SupportCtrl', function ($scope, nanopoolService, $location, $sce, $localStorage) {

    // Os Ticket Login
    $scope.osLogin = function() {
      setTimeout(function() {
        angular.element('#osticket').submit();
      }, 500);
    }

    // Check Token exists or not
    if($localStorage.token) {
      $scope.redirectURL = $sce.trustAsResourceUrl($location.search().return_url);
      $scope.token = $localStorage.token;
      $scope.osLogin();
    }
  });
