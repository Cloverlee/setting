'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:ReferralCtrl
 * @description
 * # ReferralCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .filter('secondsToDateTime', [function() {
    return function(seconds) {
      return new Date(1970, 0, 1).setSeconds(seconds);
    };
  }])
  .controller('ProductsCtrl', function ($scope,$rootScope,$timeout, $uibModal, $uibModalStack, $window, config, $stateParams, nanopoolService) {

    $scope.nano = {
      value: 100,
      nanoMining: 100,
      nanominingpower: 100,
      nanomaxunit :20,
      options: {
        showSelectionBar: true,
        getSelectionBarColor: function (value) {
          $scope.qtyPackage = value;
          if (value <= 30)
            return 'red';
          if (value <= 60)
            return 'orange';
          if (value <= 90)
            return 'yellow';
          return '#2AE02A';
        }
      }
    };



    $scope.walletHeading = config.wallet;
    $scope.currentPage = config.currentPage;
    $scope.payoutCurrentPage = config.currentPage;
    $scope.ethPoolCurrentPage = config.currentPage;
    $scope.ethMachineCurrentPage = config.currentPage;
    $scope.ethRackCurrentPage = config.currentPage;

    $scope.btcPoolCurrentPage = config.currentPage;
    $scope.btcMachineCurrentPage = config.currentPage;
    $scope.btcRackCurrentPage = config.currentPage;

    $scope.dashPoolCurrentPage = config.currentPage;
    $scope.dashMachineCurrentPage = config.currentPage;
    $scope.dashRackCurrentPage = config.currentPage;

    $scope.moneroPoolCurrentPage = config.currentPage;
    $scope.moneroMachineCurrentPage = config.currentPage;
    $scope.moneroRackCurrentPage = config.currentPage;

    $scope.viaPoolCurrentPage = config.currentPage;
    $scope.viaMachineCurrentPage = config.currentPage;
    $scope.viaRackCurrentPage = config.currentPage;

    $scope.productMaxUnit = config.productMaxUnit;
    $scope.btcImagePath = config.btcImagePath;
    $scope.ethImagePath = config.ethImagePath;
    $scope.dashImagePath = config.dashImagePath;
    $scope.moneroImagePath = config.moneroImagePath;
    $scope.viaImagePath = config.viaImagePath;
    $scope.pageLimit = config.pageLimit;
    $scope.oldPackage = config.oldPackage;

    $scope.package = ['pool', 'rack', 'machine'];

    $scope.total = 0;

    if ($stateParams.orderTab) {
      $scope.activeTab = $stateParams.orderTab;
    }

    // Get Products
    nanopoolService.getProducts()
      .then(function (res) {
        var productsData = [
  {
    "id": 1,
    "coin": "BTC",
    "productname": "Bitcoin Pool Contract",
    "miningpower": 3.00,
    "product_mining_rate": 0.00002500,
    "unit": "TH/s",
    "amount": 100.0,
    "maxunit": 1000,
    "USDPrice": 0.0923370000
  },
  {
    "id": 2,
    "coin": "BTC",
    "productname": "Bitcoin Machine Contract",
    "miningpower": 30.00,
    "product_mining_rate": 0.00025000,
    "unit": "TH/s",
    "amount": 1000.0,
    "maxunit": 100,
    "USDPrice": 0.9233700000
  },
  {
    "id": 3,
    "coin": "BTC",
    "productname": "Bitcoin Rack Contract",
    "miningpower": 300.00,
    "product_mining_rate": 0.00250000,
    "unit": "TH/s",
    "amount": 10000.0,
    "maxunit": 10,
    "USDPrice": 9.2337000000
  },
  {
    "id": 4,
    "coin": "ETH",
    "productname": "Ethereum Pool Contract",
    "miningpower": 0.58,
    "product_mining_rate": 0.00029000,
    "unit": "MH/s",
    "amount": 100.0,
    "maxunit": 1000,
    "USDPrice": 0.0775634000
  },
  {
    "id": 5,
    "coin": "ETH",
    "productname": "Ethereum Machine Contract",
    "miningpower": 5.77,
    "product_mining_rate": 0.00290000,
    "unit": "MH/s",
    "amount": 1000.0,
    "maxunit": 100,
    "USDPrice": 0.7756340000
  },
  {
    "id": 6,
    "coin": "ETH",
    "productname": "Ethereum Rack Contract",
    "miningpower": 57.66,
    "product_mining_rate": 0.02900000,
    "unit": "MH/s",
    "amount": 10000.0,
    "maxunit": 10,
    "USDPrice": 7.7563400000
  },
  {
    "id": 7,
    "coin": "BTC",
    "productname": "Bitcoin Pool Contract 2THs",
    "miningpower": 2.00,
    "product_mining_rate": 0.00002500,
    "unit": "TH/s",
    "amount": 100.0,
    "maxunit": 1000,
    "USDPrice": 0.0923370000
  },
  {
    "id": 8,
    "coin": "BTC",
    "productname": "Bitcoin Machine Contract 2THs",
    "miningpower": 20.00,
    "product_mining_rate": 0.00025000,
    "unit": "TH/s",
    "amount": 1000.0,
    "maxunit": 100,
    "USDPrice": 0.9233700000
  },
  {
    "id": 9,
    "coin": "BTC",
    "productname": "Bitcoin Rack Contract 2THs",
    "miningpower": 200.00,
    "product_mining_rate": 0.00250000,
    "unit": "TH/s",
    "amount": 10000.0,
    "maxunit": 10,
    "USDPrice": 9.2337000000
  },
  {
    "id": 10,
    "coin": "DASH",
    "productname": "Dash Pool Contract",
    "miningpower": 50.00,
    "product_mining_rate": 0.00044000,
    "unit": "MH/s",
    "amount": 100.0,
    "maxunit": 1000,
    "USDPrice": 0.1540220000
  },
  {
    "id": 11,
    "coin": "DASH",
    "productname": "Dash Machine Contract",
    "miningpower": 500.00,
    "product_mining_rate": 0.00440000,
    "unit": "MH/s",
    "amount": 1000.0,
    "maxunit": 100,
    "USDPrice": 1.5402200000
  },
  {
    "id": 12,
    "coin": "DASH",
    "productname": "Dash Rack Contract",
    "miningpower": 5000.00,
    "product_mining_rate": 0.04400000,
    "unit": "MH/s",
    "amount": 10000.0,
    "maxunit": 10,
    "USDPrice": 15.4022000000
  },
  {
    "id": 13,
    "coin": "MONERO",
    "productname": "Monero Pool Contract",
    "miningpower": 150.00,
    "product_mining_rate": 0.00170000,
    "unit": "H/s",
    "amount": 100.0,
    "maxunit": 1000,
    "USDPrice": 0.1515720000
  },
  {
    "id": 14,
    "coin": "MONERO",
    "productname": "Monero Machine Contract",
    "miningpower": 1500.00,
    "product_mining_rate": 0.01700000,
    "unit": "H/s",
    "amount": 1000.0,
    "maxunit": 100,
    "USDPrice": 1.5157200000
  },
  {
    "id": 15,
    "coin": "MONERO",
    "productname": "Monero Rack Contract",
    "miningpower": 15000.00,
    "product_mining_rate": 0.17000000,
    "unit": "H/s",
    "amount": 10000.0,
    "maxunit": 10,
    "USDPrice": 15.1572000000
  },
  {
    "id": 16,
    "coin": "VIA",
    "productname": "VIA COIN Pool Contract",
    "miningpower": 300.00,
    "product_mining_rate": 0.08940256,
    "unit": "MH/s",
    "amount": 100.0,
    "maxunit": 1000,
    "USDPrice": 0.1019189184
  },
  {
    "id": 17,
    "coin": "VIA",
    "productname": "VIA COIN Machine Contract",
    "miningpower": 3000.00,
    "product_mining_rate": 0.89402560,
    "unit": "MH/s",
    "amount": 1000.0,
    "maxunit": 100,
    "USDPrice": 1.0191891840
  },
  {
    "id": 18,
    "coin": "VIA",
    "productname": "VIA COIN Rack Contract",
    "miningpower": 30000.00,
    "product_mining_rate": 8.94025600,
    "unit": "MH/s",
    "amount": 10000.0,
    "maxunit": 10,
    "USDPrice": 10.1918918400
  }
];
        $scope.btcProducts = [];
        $scope.ethProducts = [];
        $scope.dashProducts = [];
        $scope.moneroProducts = [];
        $scope.viaProducts = [];
        if (res.status === 200) {
          var k = 0;
          productsData.forEach(function (products) {
            if (products.coin === 'BTC' && $scope.oldPackage[k] !== products.productname) {
              products.btcMining = 0;
              products.quantity = 0;
              $scope.btcProducts.push(products);
            } else if (products.coin === 'ETH' && $scope.oldPackage[k] !== products.productname) {
              products.ethMining = 0;
              products.quantity = 0;
              $scope.ethProducts.push(products);
            } else if (products.coin === 'DASH' && $scope.oldPackage[k] !== products.productname) {
              products.dashMining = 0;
              products.quantity = 0;
              $scope.dashProducts.push(products);
            } else if (products.coin === 'MONERO' && $scope.oldPackage[k] !== products.productname) {
              products.moneroMining = 0;
              products.quantity = 0;
              $scope.moneroProducts.push(products);
            }
            else if (products.coin === 'VIA' && $scope.oldPackage[k] !== products.productname) {
              products.viaMining = 0;
              products.quantity = 0;
              $scope.viaProducts.push(products);
            }
            k++;
            // $scope.total += products.amount;
            // console.log($scope.btcProducts);
          });
        }
      });

    // Get Wallet Info
    $scope.walletInfo = function () {
      nanopoolService.getWalletInfo()
        .then(function (res) {
          if (res.status === 200) {
            $scope.walletData = res.data;
          }
        });
      $scope.getTransctions();
    }

    $scope.getTransctions = function () {
      nanopoolService.getTransactionDetails($scope.currentPage)
        .then(function (res) {
          if (res === 200) {
            $scope.transactionDetails = res.data;
            console.log($scope.transactionDetails);
          }
        })
    }

    // Get Mining Payouts
    $scope.miningPayouts = function (payoutCurrentPage) {
      nanopoolService.getPayouts(payoutCurrentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.payoutDetails = data.rows;
            $scope.payoutTotal = data.total;
            if ($scope.payoutTotal === 0) {
              $scope.noPayout = true;
            }
          }
        })
    }

    $scope.setBtcQuantity = function (key, val) {
      $scope.btcProducts[key].quantity = val;
    }

    $scope.calculateBtcAmount = function (key, quantity, miningpower, maxUnit) {
      if (quantity > maxUnit) {
        $scope.btcProducts[key].quantity = maxUnit;
        $scope.btcProducts[key].btcMining = maxUnit * miningpower;
        angular.element("#quantity-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.btcProducts[key].btcMining + '<small>TH/s</small>');
        return false;
      } else {
        $scope.btcProducts[key].btcMining = quantity * miningpower;
        angular.element("#quantity-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.btcProducts[key].btcMining + '<small>TH/s</small>');
      }
    }

    $scope.calculateEthAmount = function (key, quantity, miningpower, maxUnit) {
      if (quantity > maxUnit) {
        $scope.ethProducts[key].quantity = maxUnit;
        $scope.ethProducts[key].ethMining = maxUnit * miningpower;
        angular.element("#quantity-eth-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.ethProducts[key].ethMining + '<small>MH/s</small>');
        return false;
      } else {
        $scope.ethProducts[key].ethMining = quantity * miningpower;
        angular.element("#quantity-eth-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.ethProducts[key].ethMining + '<small>MH/s</small>');
      }
    }

    $scope.calculateDashAmount = function (key, quantity, miningpower, maxUnit) {
      if (quantity > maxUnit) {
        $scope.dashProducts[key].quantity = maxUnit;
        $scope.dashProducts[key].dashMining = maxUnit * miningpower;
        angular.element("#quantity-dash-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.dashProducts[key].dashMining + '<small>MH/s</small>');
        return false;
      } else {
        $scope.dashProducts[key].dashMining = quantity * miningpower;
        angular.element("#quantity-dash-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.dashProducts[key].dashMining + '<small>MH/s</small>');
      }
    }

    $scope.calculateMoneroAmount = function (key, quantity, miningpower, maxUnit) {
      if (quantity > maxUnit) {
        $scope.moneroProducts[key].quantity = maxUnit;
        $scope.moneroProducts[key].moneroMining = maxUnit * miningpower;
        angular.element("#quantity-monero-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.moneroProducts[key].moneroMining + '<small>H/s</small>');
        return false;
      } else {
        $scope.moneroProducts[key].moneroMining = quantity * miningpower;
        angular.element("#quantity-monero-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.moneroProducts[key].moneroMining + '<small>H/s</small>');
      }
    }

    $scope.calculateViaAmount = function (key, quantity, miningpower, maxUnit) {
      console.log(quantity);
      if (quantity > maxUnit) {
        $scope.viaProducts[key].quantity = maxUnit;
        $scope.viaProducts[key].viaMining = maxUnit * miningpower;
        angular.element("#quantity-via-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.viaProducts[key].viaMining + '<small>MH/s</small>');
        return false;
      } else {
        $scope.viaProducts[key].viaMining = quantity * miningpower;
        angular.element("#quantity-via-" + key).parent().find(".price-slider .ui-slider-handle > label").html($scope.viaProducts[key].viaMining + '<small>MH/s</small>');
      }
    }

    $scope.setEthQuantity = function (key, val) {
      $scope.ethProducts[key].quantity = val;
    }

    $scope.setDashQuantity = function (key, val) {
      $scope.dashProducts[key].quantity = val;
    }

    $scope.setMoneroQuantity = function (key, val) {
      $scope.moneroProducts[key].quantity = val;
    }

    $scope.setViaQuantity = function (key, val) {
      $scope.viaProducts[key].quantity = val;
    }

    // Close Modal
    $scope.closeModal = function () {
      $uibModalStack.dismissAll();
      $window.location.reload();
    }

    // Select Payment Mode
    $scope.selectPayment = function (amount) {
      $scope.noOrder = false;
      $scope.payment = {
        mode: "BTC"
      }

      nanopoolService.getBuyNanoToken($scope.qtyPackage)
        .then(function(res) {
          var data = res.data;
      /*    console.log(res);*/
          /*if(res.status === 200) {
            $scope.hasSuccess = true;
          }else{
            $scope.resetPassError = data.Message;
          }*/
        });

      $scope.orderDetails = [];
      var i = 0, j = 0, k = 0, l = 0, m = 0, n = 0;
  /*    console.log($scope.dashProducts);*/
      $scope.dashProducts.forEach(function (dashInfo) {
        if (dashInfo.quantity !== 0 && dashInfo.quantity !== null) {
          var dashAmount = dashInfo.amount * dashInfo.dashMining / dashInfo.miningpower;
          $scope.purchaseTotal += dashAmount;
          $scope.orderDetails.push({ data: { id: dashInfo.id, quantity: dashInfo.quantity }, name: dashInfo.productname, price: dashAmount, path: $scope.dashImagePath[k] });
        }
        k++;
      });

      $scope.btcProducts.forEach(function (btcInfo) {
        if (btcInfo.quantity !== 0 && btcInfo.quantity !== null) {
          var btcAmount = btcInfo.amount * btcInfo.btcMining / btcInfo.miningpower;
          $scope.purchaseTotal += btcAmount;
          $scope.orderDetails.push({ data: { id: btcInfo.id, quantity: btcInfo.quantity }, name: btcInfo.productname, price: btcAmount, path: $scope.btcImagePath[i] });
        }
        i++;
      });

      $scope.ethProducts.forEach(function (ethInfo) {
        if (ethInfo.quantity !== 0 && ethInfo.quantity !== null) {
          var ethAmount = ethInfo.amount * ethInfo.ethMining / ethInfo.miningpower;
          $scope.purchaseTotal += ethAmount;
          $scope.orderDetails.push({ data: { id: ethInfo.id, quantity: ethInfo.quantity }, name: ethInfo.productname, price: ethAmount, path: $scope.ethImagePath[j] });
        }
        j++;
      });

      $scope.moneroProducts.forEach(function (moneroInfo) {
        if (moneroInfo.quantity !== 0 && moneroInfo.quantity !== null) {
          var moneroAmount = moneroInfo.amount * moneroInfo.moneroMining / moneroInfo.miningpower;
          $scope.purchaseTotal += moneroAmount;
          $scope.orderDetails.push({ data: { id: moneroInfo.id, quantity: moneroInfo.quantity }, name: moneroInfo.productname, price: moneroAmount, path: $scope.moneroImagePath[j] });
        }
        l++;
      });

      $scope.viaProducts.forEach(function (viaInfo) {
        if (viaInfo.quantity !== 0 && viaInfo.quantity !== null) {
          var viaAmount = viaInfo.amount * viaInfo.viaMining / viaInfo.miningpower;
          $scope.purchaseTotal += viaAmount;
          $scope.orderDetails.push({ data: { id: viaInfo.id, quantity: viaInfo.quantity }, name: viaInfo.productname, price: viaAmount, path: $scope.viaImagePath[j] });
        }
        m++;
      });

      $scope.orderParams = [];
      $scope.orderDetails.forEach(function (info) {
        if (info.data.quantity !== undefined && info.data.quantity !== null) {
          $scope.orderParams.push(info.data);
        }
      });


      if ($scope.orderParams.length > 0) {
        var modalInstance = $uibModal.open({
          templateUrl: 'views/modal/purchase-contract.html',
          scope: $scope,
          size: 'md'
        });
      } else {
        /*$scope.noOrder = true;*/
        console.log($scope.noOrder);
      }


    }
//Open modal
    $scope.addFund1 = function(amount) {
      $rootScope.addAmount = amount;
      $scope.loadingData = true;
      nanopoolService.addUsdFund(amount)
        .then(function(res) {
          console.log(res);
          if(res.status === 200) {
            $scope.loadingData = false;
            angular.element("#add-funds").hide();
            var data = res.data;
            console.log('res if');
            $scope.transactionDetails = data;
            $scope.transactionDate = moment().format('YYYY-MM-DD');
            var modalInstance = $uibModal.open({
              templateUrl: 'views/transaction-invoice.html',
              scope: $scope,
              size: 'lg'
            });
            $scope.name = 'Superhero';
            $scope.counter = 1800;
            $scope.onTimeout = function(){
              if ($scope.counter > 0) $scope.counter--;
              mytimeout = $timeout($scope.onTimeout,1000);
            }
            var mytimeout = $timeout($scope.onTimeout,1000);

          }
        })
    }

    // Close Pop Up
    $scope.closePopup = function() {
      $uibModalStack.dismissAll();
      $window.location.reload();
    }
    $scope.printInvoice = function () {
      $window.print();
    }

    $scope.purchaseTotal = 0;

    $scope.bookOrder = function (payment) {
      $scope.loadingData = true;

      var orderData = {
        "payment_method": payment.mode,
        "payment_type": "",
        "OrderDetails": $scope.orderParams
      }

      // console.log(orderData);
      // Book Order
      nanopoolService.bookOrder(orderData).then(function (res) {
        $uibModalStack.dismissAll();
        $scope.loadingData = false;
        if (res.status === 200) {
          var data = res.data;
          if (data.message && data.message !== 'success') {
            $scope.noBalance = true;
          } else {
            $scope.purchase = data;
            $scope.orderDetails = $scope.orderDetails;
          }

          $scope.transactionDate = moment().format('YYYY-MM-DD');

          var modalInstance = $uibModal.open({
            templateUrl: 'views/modal/purchase-invoice.html',
            scope: $scope,
            size: 'md',
            backdrop: 'static'
          });
        }
      })
    }

    // Get Purchase History
    $scope.purchaseEthPool = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.ethPoolPower = data.TotalPower;
            $scope.ethPoolEstimateIncome = data.estimated_total_income;
            $scope.ethPoolCurrentRate = data.current_rate;
            $scope.ethPoolRecords = data.records.total;
            $scope.ethPoolDetails = data.records.rows;
            if ($scope.ethPoolRecords === 0) {
              $scope.noEthPoolRecords = true;
            }
          }
        })
    }

    $scope.purchaseEthMachine = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.ethMachinePower = data.TotalPower;
            $scope.ethMachineEstimateIncome = data.estimated_total_income;
            $scope.ethMachineCurrentRate = data.current_rate;
            $scope.ethMachineRecords = data.records.total;
            $scope.ethMachineDetails = data.records.rows;
            if ($scope.ethMachineRecords === 0) {
              $scope.noEthMachineRecords = true;
            }
          }
        })
    }

    $scope.purchaseEthRack = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.ethRackPower = data.TotalPower;
            $scope.ethRackEstimateIncome = data.estimated_total_income;
            $scope.ethRackCurrentRate = data.current_rate;
            $scope.ethRackRecords = data.records.total;
            $scope.ethRackDetails = data.records.rows;
            if ($scope.ethRackRecords === 0) {
              $scope.noEthRackRecords = true;
            }
          }
        })
    }

    $scope.purchaseDashPool = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.dashPoolPower = data.TotalPower;
            $scope.dashPoolEstimateIncome = data.estimated_total_income;
            $scope.dashPoolCurrentRate = data.current_rate;
            $scope.dashPoolRecords = data.records.total;
            $scope.dashPoolDetails = data.records.rows;
            if ($scope.dashPoolRecords === 0) {
              $scope.noDashPoolRecords = true;
            }
          }
        })
    }

    $scope.purchaseDashMachine = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.dashMachinePower = data.TotalPower;
            $scope.dashMachineEstimateIncome = data.estimated_total_income;
            $scope.dashMachineCurrentRate = data.current_rate;
            $scope.dashMachineRecords = data.records.total;
            $scope.dashMachineDetails = data.records.rows;
            if ($scope.dashMachineRecords === 0) {
              $scope.noDashMachineRecords = true;
            }
          }
        })
    }

    $scope.purchaseDashRack = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.dashRackPower = data.TotalPower;
            $scope.dashRackEstimateIncome = data.estimated_total_income;
            $scope.dashRackCurrentRate = data.current_rate;
            $scope.dashRackRecords = data.records.total;
            $scope.dashRackDetails = data.records.rows;
            if ($scope.dashRackRecords === 0) {
              $scope.noDashRackRecords = true;
            }
          }
        })
    }

    // Get Purchase History
    $scope.purchaseBtcPool = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.btcPoolPower = data.TotalPower;
            $scope.btcPoolEstimateIncome = data.estimated_total_income;
            $scope.btcPoolCurrentRate = data.current_rate;
            $scope.btcPoolRecords = data.records.total;
            $scope.btcPoolDetails = data.records.rows;
            if ($scope.btcPoolRecords === 0) {
              $scope.nobtcPoolRecords = true;
            }
          }
        })
    }

    $scope.purchaseBtcMachine = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.btcMachinePower = data.TotalPower;
            $scope.btcMachineEstimateIncome = data.estimated_total_income;
            $scope.btcMachineCurrentRate = data.current_rate;
            $scope.btcMachineRecords = data.records.total;
            $scope.btcMachineDetails = data.records.rows;
            if ($scope.btcMachineRecords === 0) {
              $scope.nobtcMachineRecords = true;
            }
          }
        })
    }

    $scope.purchaseBtcRack = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.btcRackPower = data.TotalPower;
            $scope.btcRackEstimateIncome = data.estimated_total_income;
            $scope.btcRackCurrentRate = data.current_rate;
            $scope.btcRackRecords = data.records.total;
            $scope.btcRackDetails = data.records.rows;
            if ($scope.btcRackRecords === 0) {
              $scope.nobtcRackRecords = true;
            }
          }
        })
    }

    $scope.purchaseMoneroPool = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.moneroPoolPower = data.TotalPower;
            $scope.moneroPoolEstimateIncome = data.estimated_total_income;
            $scope.moneroPoolCurrentRate = data.current_rate;
            $scope.moneroPoolRecords = data.records.total;
            $scope.moneroPoolDetails = data.records.rows;
            if ($scope.moneroPoolRecords === 0) {
              $scope.nomoneroPoolRecords = true;
            }
          }
        })
    }

    $scope.purchaseMoneroMachine = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.moneroMachinePower = data.TotalPower;
            $scope.moneroMachineEstimateIncome = data.estimated_total_income;
            $scope.moneroMachineCurrentRate = data.current_rate;
            $scope.moneroMachineRecords = data.records.total;
            $scope.moneroMachineDetails = data.records.rows;
            if ($scope.moneroMachineRecords === 0) {
              $scope.nomoneroMachineRecords = true;
            }
          }
        })
    }

    $scope.purchaseMoneroRack = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.moneroRackPower = data.TotalPower;
            $scope.moneroRackEstimateIncome = data.estimated_total_income;
            $scope.moneroRackCurrentRate = data.current_rate;
            $scope.moneroRackRecords = data.records.total;
            $scope.moneroRackDetails = data.records.rows;
            if ($scope.moneroRackRecords === 0) {
              $scope.nomoneroRackRecords = true;
            }
          }
        })
    }

    $scope.purchaseViaPool = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.viaPoolPower = data.TotalPower;
            $scope.viaPoolEstimateIncome = data.estimated_total_income;
            $scope.viaPoolCurrentRate = data.current_rate;
            $scope.viaPoolRecords = data.records.total;
            $scope.viaPoolDetails = data.records.rows;
            if ($scope.viaPoolRecords === 0) {
              $scope.noViaPoolRecords = true;
            }
          }
        })
    }

    $scope.purchaseViaMachine = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.viaMachinePower = data.TotalPower;
            $scope.viaMachineEstimateIncome = data.estimated_total_income;
            $scope.viaMachineCurrentRate = data.current_rate;
            $scope.viaMachineRecords = data.records.total;
            $scope.viaMachineDetails = data.records.rows;
            if ($scope.viaMachineRecords === 0) {
              $scope.noViaMachineRecords = true;
            }
          }
        })
    }

    $scope.purchaseViaRack = function (type, product, currentPage) {
      nanopoolService.orderHistory(type, product, currentPage)
        .then(function (res) {
          if (res.status === 200) {
            var data = res.data;
            $scope.viaRackPower = data.TotalPower;
            $scope.viaRackEstimateIncome = data.estimated_total_income;
            $scope.viaRackCurrentRate = data.current_rate;
            $scope.viaRackRecords = data.records.total;
            $scope.viaRackDetails = data.records.rows;
            if ($scope.viaRackRecords === 0) {
              $scope.noViaRackRecords = true;
            }
          }
        })
    }

    $scope.purchaseEthPool('ETH', 'pool', $scope.ethPoolCurrentPage);
    $scope.purchaseEthMachine('ETH', 'machine', $scope.ethMachineCurrentPage);
    $scope.purchaseEthRack('ETH', 'rack', $scope.ethRackCurrentPage);

    $scope.purchaseDashPool('DASH', 'pool', $scope.dashPoolCurrentPage);
    $scope.purchaseDashMachine('DASH', 'machine', $scope.dashMachineCurrentPage);
    $scope.purchaseDashRack('DASH', 'rack', $scope.dashRackCurrentPage);

    $scope.purchaseBtcPool('BTC', 'pool', $scope.btcPoolCurrentPage);
    $scope.purchaseBtcMachine('BTC', 'machine', $scope.btcMachineCurrentPage);
    $scope.purchaseBtcRack('BTC', 'rack', $scope.btcRackCurrentPage);

    $scope.purchaseMoneroPool('MONERO', 'pool', $scope.moneroPoolCurrentPage);
    $scope.purchaseMoneroMachine('MONERO', 'machine', $scope.moneroMachineCurrentPage);
    $scope.purchaseMoneroRack('MONERO', 'rack', $scope.moneroRackCurrentPage);

    $scope.purchaseViaPool('VIA', 'pool', $scope.viaPoolCurrentPage);
    $scope.purchaseViaMachine('VIA', 'machine', $scope.viaMachineCurrentPage);
    $scope.purchaseViaRack('VIA', 'rack', $scope.viaRackCurrentPage);

  });
