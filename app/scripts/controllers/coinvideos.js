'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:CoinvideosCtrl
 * @description
 * # CoinvideosCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('CoinvideosCtrl', function ($scope, $stateParams) {
    
    if($stateParams.tabId) {
      $scope.activeTab = $stateParams.tabId;
    }
  
});
