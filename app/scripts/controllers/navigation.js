'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
.controller('NavCtrl', function ($scope, $cookies, $state, $window, $rootScope, $localStorage, $timeout, nanopoolService, UtilsService) {

 //  Logged out User
 $scope.logout = function() {
   if($localStorage.token || $cookies.get('token')) {
     $localStorage.$reset();
     $cookies.remove('token');
     $state.go('login');
     $window.location.reload();
   }
 }

 $rootScope.$on('logout', function() {
   $scope.logout();
   $window.location.reload();
 });
});
