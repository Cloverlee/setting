'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:navigation
 * @description
 * # navigation
 */
angular.module('nanopoolFrontendApp')
  .directive('navigation', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/common/navigation.html',
      scope: true,
      controller: 'NavCtrl'
    };
  });
