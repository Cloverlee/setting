'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:userProfile
 * @description
 * # userProfile
 */
angular.module('nanopoolFrontendApp')
  .directive('userProfile', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/directiveTemplates/user-profile.html',
      scope: {
        'userInfo': '=',
        'parentSponsor': '=',
        'dialCode':'=',
        'errorMessage':'=',
        'successMessage':'=',
        'updateProfile':'&',
        'customClass': '@'
      },
    };
  });
