'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:changePassword
 * @description
 * # changePassword
 */
angular.module('nanopoolFrontendApp')
  .directive('changePassword', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/directiveTemplates/changePassword.html',
      scope: {
        'userValue':'=',
        'confirmPassError':'=',
        'errorMessage':'=',
        'successMessage':'=',
        'changeUserPassword':'&',
        'confirmPass':'&'
      },
    };
  });
