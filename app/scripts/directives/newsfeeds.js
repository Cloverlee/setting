'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:newsFeeds
 * @description
 * # newsFeeds
 */
angular.module('nanopoolFrontendApp')
  .directive('newsFeeds', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/directiveTemplates/newsFeeds.html'
    };
  });
